<div id="metasidebar">
<div id="sidebar">

<!-- hardcoded sidebar-->
<ul>
	<li>Metabiosis
		<ul>
		<li><a href="<?php bloginfo('home'); ?>/about/">about</a></li>
		<li><a href="<?php bloginfo('home'); ?>/contact/">contact</a></li>
		</ul>
	</li>
	
	<li>Nodes
		<ul>
		<li><a href="<?php bloginfo('home'); ?>/go-forth/">go forth &amp; *</a></li>
		<li><a href="<?php bloginfo('home'); ?>/herbarium-workshops/">Herbarium</a></li>
		<li><a href="<?php bloginfo('home'); ?>/d_u_w/">d_u_w</a></li>
		<li><a href="<?php bloginfo('home'); ?>/pond/">pond</a></li>
		<li><a href="<?php bloginfo('home'); ?>/meshy/">meshy</a></li>
		<li><a href="<?php bloginfo('home'); ?>/hello-process/">hello process!</a></li>
		</ul>
	</li>
	
	<?php wp_list_categories('title_li=' . __('tags')); ?>

	<li id="archives"><?php _e('archives:'); ?>
	<ul><?php wp_get_archives('type=yearly'); ?></ul>
	</li>

		<li id="search">
		<label for="s"><?php _e('Search:'); ?></label>
		<form id="searchform" method="get" action="<?php bloginfo('home'); ?>">
		<div>
		<input type="text" name="s" id="s" size="15" /><br />
		<input type="submit" id="sbutton" value="<?php _e('Search'); ?>" />
		</div>
		</form>
	</li>
</ul>

</div>
</div>
