<?php get_header(); ?>

	<div id="content">
				
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<!--<div class="navigation">
			<div class="alignleft"><?php previous_post_link('&laquo; %link') ?></div>
			<div class="alignright"><?php next_post_link('%link &raquo;') ?></div>
		</div>-->
	
		<div class="post" id="post-<?php the_ID(); ?>">
			<h2><a href="<?php echo get_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_title(); ?></a></h2>
			<small> <?php the_category(', ') ?> - <?php the_time('F jS, Y') ?> - <?php the_author() ?> - <?php edit_post_link('Edit', '', ' - '); ?>  <?php comments_popup_link('No Comments', '1 Comment &#187;', '% Comments &#187;'); ?></small>
						
			<div class="entrytext"><?php the_content('<p class="serif">Read the rest of this entry &raquo;</p>'); ?>
				<?php link_pages('<p><strong>Pages:</strong> ', '</p>', 'number'); ?>
			</div>
	
    <?php comment_form(); ?>	
	<?php comments_template(); ?>
	</div>
	<?php endwhile; else: ?>
		<p>Sorry, no posts matched your criteria.</p>
	
<?php endif; ?>
	
	</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
